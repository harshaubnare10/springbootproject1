package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
@Controller
public class MainController {
 @Autowired
 private EmployeeService employeeservice;
    @GetMapping("test")
    public Map test(){
        Map a = new HashMap();
        a.put("name","harsha");
        return a;
    }
    @RequestMapping(method = RequestMethod.GET, value = "/")
    @ResponseBody
    public ModelAndView index () {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }
    @RequestMapping(method = RequestMethod.GET, value = "/registration")
    @ResponseBody
    public ModelAndView index1 () {
        ModelAndView modelAndView1 = new ModelAndView();
        modelAndView1.setViewName("registration");
        return modelAndView1;
    }
    @GetMapping("/save")
public String saveEmployee(@RequestParam String name,@RequestParam String age,@RequestParam String email,@RequestParam String password,@RequestParam String address)
{
    Employee emp=new Employee(name,age,email,password,address);
    employeeservice.saveMyEmployeee(emp);
    return "employee information is saved";
}

}
